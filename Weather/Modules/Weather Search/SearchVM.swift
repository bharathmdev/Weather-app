//
//  SearchVM.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import Foundation

class SearchVM {
    var searchResultList: [SearchResultModel]?
    
    var reloadUI: () -> Void = { }
   // var requests: [Cancellable]?
    var isLoading: Bool = false {
        didSet {
            self.reloadUI()
        }
    }

    func performSearch(text: String) {
        
        guard !text.isEmpty else {
            self.searchResultList?.removeAll()
            self.reloadUI()
            return
        }

        self.isLoading = true
        let networkManager = NetworkManager()
        networkManager.getModel(request: NetworkParams.seachText(text).urlRequest) {[weak self]  (result: Result<[SearchResultModel], Error>) in
            switch result {
            case .success(let result):
                DispatchQueue.main.async {
                    self?.searchResultList = result
                  //  completion(result, nil)
                    self?.isLoading = false
                    print(result)
                }

            case .failure(let error):
                DispatchQueue.main.async {
                    self?.searchResultList =  []
               // completion(nil, error)
                }
            }
        }
        
//        guard !text.isEmpty else {
//            self.searchResultList?.removeAll()
//            self.reloadUI()
//            return
//        }
//        isLoading = true
//        // Make API call
//        requests?.forEach({$0.cancel()})
//        requests?.removeAll()
//        let request = WeatherAPI.provider.request(.seachText(text: text)) {
//            [weak self] result in
//            switch result{
//            case .success(let response):
//                let decoder = JSONDecoder()
//                // Populating searchResultList variable
//                self?.searchResultList = try? decoder.decode(
//                    [SearchResultModel].self,
//                    from: response.data
//                )
//                self?.isLoading = false
//            case .failure(let error):
//                print(error)
//            }
//        }
//        requests?.append(request)
    }
    
}


