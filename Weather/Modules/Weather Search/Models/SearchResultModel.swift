//
//  SearchResultModel.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import Foundation

struct SearchResultModel: Codable {
    let name: String?
    let lat: Double?
    let lon: Double?
    let country: String?
    let state: String?
}
