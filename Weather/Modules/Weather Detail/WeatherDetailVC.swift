//
//  WeatherDetailVC.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import UIKit

class WeatherDetailVC: UIViewController {
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureText: UILabel!
    @IBOutlet weak var feelsLikeText: UILabel!
    @IBOutlet weak var weatherTitle: UILabel!
    @IBOutlet weak var weatherSubtitle: UILabel!
    
    
    var viewModel = WeatherDetailVM()
    var coordinator: RootCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func setLocation(with location: SearchResultModel) {
        self.viewModel.delegate = self
        self.viewModel.location = location
    }
    
}

extension WeatherDetailVC: WeatherDetailVMDelegate {
    func startLoading() {
        LoadingIndicatorView.show("Loading")
    }
    
    func stopLoading() {
        LoadingIndicatorView.hide()
    }
    
    func weatherLoaded() {
        guard let weather = viewModel.weatherData else { return }
        self.cityLabel.text = weather.name
        self.weatherIcon.image = UIImage(named: "weather")
        self.weatherTitle.text = weather.weather?.first?.main
        self.weatherSubtitle.text = weather.weather?.first?.description
        guard let temperature = weather.main?.temp else { return }
        self.temperatureText.text = String(describing: temperature) + " °C"
        
        if let feelsLike = weather.main?.feelsLike {
        self.feelsLikeText.text = "Feels like " + String(describing: feelsLike) + " °C"
        }
        else {
            self.feelsLikeText.text = " "
        }
        
    }
    
}

