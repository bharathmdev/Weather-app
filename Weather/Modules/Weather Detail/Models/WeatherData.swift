//
//	WeatherData.swift
//	Model file generated using JSONExport

import Foundation

struct WeatherData : Codable {


	let main : Main?
	let name : String?
	let weather : [Weather]?


}
