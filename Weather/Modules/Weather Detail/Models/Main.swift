//
//	Main.swift
//	Model file generated using JSONExport

import Foundation

struct Main : Codable {

	let feelsLike : Float?
	let humidity : Int?
	let pressure : Int?
	let temp : Float?
	let tempMax : Float?
	let tempMin : Float?


}
