//
//  WeatherDetailVM.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import Foundation

protocol WeatherDetailVMDelegate {
    func startLoading()
    func weatherLoaded()
    func stopLoading()
}

class WeatherDetailVM {
    
    var delegate: WeatherDetailVMDelegate?
    
    var weatherData: WeatherData?
    
    var location: SearchResultModel? {
        didSet {
            self.fetchWeather()
        }
    }
    
    func fetchWeather() {
        self.delegate?.startLoading()
        // API call to fetch weather
        // TODO: Handle error cases properly
            let networkManager = NetworkManager()
        networkManager.getModel(request: NetworkParams.getWeather(location?.lat ?? 0.0, location?.lon ?? 0.0).urlRequest) { [weak self] (result: Result<WeatherData, Error>) in
                switch result {
                case .success(let result):
                    DispatchQueue.main.async {
                        self?.weatherData = result
                      //  completion(result, nil)
                        self?.delegate?.weatherLoaded()
                        self?.delegate?.stopLoading()
                        print(result)
                    }

                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.weatherData = nil
                   // completion(nil, error)
                    }
                }
            }
     
    }
}
