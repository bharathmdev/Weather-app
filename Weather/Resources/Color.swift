//
//  Color.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import Foundation
import UIKit

// ENUM returning colors stored in Assets.xcassets colors
enum Color {
    
    static var darkestText: UIColor {
        return UIColor(named: "darkest.text")!
    }
    
    static var darkerText: UIColor {
        return UIColor(named: "darker.text")!
    }
    
    static var darkText: UIColor {
        return UIColor(named: "dark.text")!
    }
    
    
}
