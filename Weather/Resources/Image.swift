//
//  Image.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import UIKit

// ENUM returning images stored in Assets.xcassets images
enum Image {
    
    static var caratArrow: UIImage {
        return UIImage.init(named: "carat")!
    }
    
    static var emptyResults: UIImage {
        return UIImage.init(named: "emptyResult")!
    }
}

