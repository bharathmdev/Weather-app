//
//  WeatherAPI.swift
//  Weather
//
//  Created by bharath on 06/01/2023
//

import Foundation

enum NetworkError: Error, Equatable {
    case missingData
    case badServerResponse(Int)
    case badRequest
    case emptyResult
}

protocol Session {
    func requestData(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}

extension URLSession: Session {
    func requestData(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.dataTask(with: request) { data, response, error in
            completion(data, response, error)
        }.resume()
    }
}

protocol WeatherAPI {
    var session: Session { get }
    func getModel<T: Decodable>(request: URLRequest?, completion: @escaping (Result<T, Error>) -> Void)
}

class NetworkManager: WeatherAPI {
    var session: Session
    
    init(session: Session = URLSession.shared) {
        self.session = session
    }
    
    func getModel<T: Decodable>(request: URLRequest?, completion: @escaping (Result<T, Error>) -> Void) {
        guard let request = request else {
            completion(.failure(NetworkError.badRequest))
            return
        }
        self.session.requestData(request: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            if let httpResponse = response as? HTTPURLResponse, !(200..<300).contains(httpResponse.statusCode) {
                completion(.failure(NetworkError.badServerResponse(httpResponse.statusCode)))
                return
            }
            guard let data = data else {
                completion(.failure(NetworkError.missingData))
                return
            }
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(.success(model ))
            } catch {
                completion(.failure(error))
            }
        }
        return
    }
    
}

enum NetworkParams {
    private enum NetworkConstants: String {
        case baseURL = "https://api.openweathermap.org/"
        case apiKey = "a5ec54756a2a502bc2d9c024f40d18f6"
    }
    
    case seachText(String)
    case getWeather(Double, Double)
    
    var urlRequest: URLRequest? {
        switch self {
        
        case .seachText(let searchQuery) :
            let  params: [String : String]? = ["q": searchQuery.replacingOccurrences(of: " ", with: ""),"limit": "12"]
            
            var queryItems = [URLQueryItem(name: "appid", value: NetworkConstants.apiKey.rawValue)]
            if let params = params {
                queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
            }
            var components = URLComponents(string: NetworkConstants.baseURL.rawValue+"geo/1.0/direct")
            components?.queryItems = queryItems
            guard let url = components?.url else { return nil }
            return URLRequest(url: url)
            
        case .getWeather(let lat, let long) :
            let  params: [String : String]? = ["lat": "\(lat)", "lon": "\(long)", "units": "metric",]
            
            var queryItems = [URLQueryItem(name: "appid", value: NetworkConstants.apiKey.rawValue)]
            if let params = params {
                queryItems.append(contentsOf: params.map { URLQueryItem(name: $0.key, value: $0.value) })
            }
            var components = URLComponents(string: NetworkConstants.baseURL.rawValue+"data/2.5/weather")
            components?.queryItems = queryItems
            guard let url = components?.url else { return nil }
            return URLRequest(url: url)
            
            
        }
        
        
    }
    
    
}

